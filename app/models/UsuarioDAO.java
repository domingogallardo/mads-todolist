package models;

import play.*;
import play.mvc.*;
import play.db.jpa.*;
import java.util.List;
import java.util.Date;

import javax.persistence.*;

public class UsuarioDAO {
    public static Usuario create (Usuario usuario) {
        usuario.nulificaAtributos();
        JPA.em().persist(usuario);
        // Hacemos un flush y un refresh para asegurarnos de que se realiza
        // la creación en la BD y se devuelve el id inicializado
        JPA.em().flush();
        JPA.em().refresh(usuario);
        Logger.debug(usuario.toString());
        return usuario;
    }

    public static Usuario find(Integer idUsuario) {
        return JPA.em().find(Usuario.class, idUsuario);
    }

    public static Usuario update(Usuario usuario) {
        return JPA.em().merge(usuario);
    }

    public static void delete(Integer idUsuario) {
        Usuario usuario = JPA.em().getReference(Usuario.class, idUsuario);
        JPA.em().remove(usuario);
    }

    public static List<Usuario> findAll() {
        return (List<Usuario>) JPA.em().createQuery("select u from Usuario u ORDER BY id").getResultList();
    }

    public static Usuario findByLogin(String login) {
        TypedQuery<Usuario> query = JPA.em().createQuery("SELECT u FROM Usuario u WHERE u.login= :login",
            Usuario.class);
        try {
            Usuario usuario = query.setParameter("login", login).getSingleResult();
            return usuario;
        } catch (NoResultException exception) {
            return null;
        }
    }
}
