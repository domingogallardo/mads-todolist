package models;

import play.*;
import play.mvc.*;
import play.db.jpa.*;
import java.util.List;

public class TareaService {
    public static List<Tarea> findAllTareasUsuario(Integer usuarioId) {
        Usuario usuario = UsuarioDAO.find(usuarioId);
        return usuario.tareas;
    }
}
