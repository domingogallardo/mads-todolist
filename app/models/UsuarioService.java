package models;

import play.*;
import play.mvc.*;
import play.db.jpa.*;
import java.util.List;
import java.util.Date;

public class UsuarioService {

    // Crea un nuevo usuario
    // Lanza una excepción si el login ya existe
    public static Usuario grabaUsuario(Usuario usuario) {
        if (findUsuarioByLogin(usuario.login) != null) {
            throw new RuntimeException("Login ya existente");
        }
        return UsuarioDAO.create(usuario);
    }

    public static Usuario modificaUsuario(Usuario usuario) {
        return UsuarioDAO.update(usuario);
    }

    public static Usuario findUsuario(Integer id) {
        return UsuarioDAO.find(id);
    }

    public static Usuario findUsuarioByLogin(String login) {
        return UsuarioDAO.findByLogin(login);
    }

    public static boolean deleteUsuario(Integer id) {
        Usuario usuario = UsuarioDAO.find(id);
        if (usuario != null) {
            UsuarioDAO.delete(id);
            return true;
        } else return false;
    }

    public static List<Usuario> findAllUsuarios() {
        return UsuarioDAO.findAll();
    }
}
