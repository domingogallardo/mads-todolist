package controllers;

import java.util.List;

import play.*;
import play.mvc.*;
import views.html.*;
import static play.libs.Json.*;
import play.data.Form;
import play.db.jpa.*;

import models.*;

public class Usuarios extends Controller {

    /*
    * Login y registro
    *
    */

    // Devuelve un formulario para realizar un login
    public Result formularioLogin() {
        return ok(formLogin.render(Form.form(Login.class),""));
    }

    @Transactional(readOnly = true)
    // Valida el login y la contraseña de un usuario
    public Result doLoginUsuario() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(formLogin.render(loginForm, "Hay errores en el formulario"));
        }
        Login login = loginForm.get();
        if (login.login.equals("admin") && login.password.equals("admin")) {
            return redirect(controllers.routes.Usuarios.listaUsuarios());
        }
        Usuario usuario = UsuarioService.findUsuarioByLogin(login.login);
        if (usuario == null) {
            return badRequest(formLogin.render(loginForm, "No existe el usuario"));
        }
        if (!usuario.password.equals(login.password)) {
            return badRequest(formLogin.render(loginForm, "Contraseña incorrecta"));
        }
        return redirect(controllers.routes.Tareas.listaTareas(usuario.id));
    }

    public Result formularioRegistro() {
        return ok(formRegistroUsuario.render(Form.form(Registro.class),""));
    }

    @Transactional
    // Añade un nuevo usuario en la BD y devuelve código HTTP
    // de redirección a la página de listado de usuarios
    public Result registraNuevoUsuario() {
        Form<Registro> registroForm = Form.form(Registro.class).bindFromRequest();
        if (registroForm.hasErrors()) {
            return badRequest(formRegistroUsuario.render(registroForm, "Hay errores en el formulario"));
        }
        Registro registro = registroForm.get();
        if (UsuarioService.findUsuarioByLogin(registro.login) != null) {
            return badRequest(formRegistroUsuario.render(registroForm, "Error: el login '" +
                                                                      registro.login +
                                                                      "' ya existe"));
        }
        Usuario usuario = new Usuario();
        usuario.login = registro.login;
        usuario.password = registro.password;
        usuario.eMail = registro.eMail;
        usuario = UsuarioService.grabaUsuario(usuario);
        return redirect(controllers.routes.Tareas.listaTareas(usuario.id));
    }

    /*
    *  CRUD de usuarios para administrador
    *
    */

    @Transactional(readOnly = true)
    // Devuelve una página con la lista de usuarios
    public Result listaUsuarios() {
        // Obtenemos el mensaje flash guardado en la petición
        // por el controller grabaUsuario
        String mensaje = flash("grabaUsuario");
        List<Usuario> usuarios = UsuarioService.findAllUsuarios();
        return ok(listaUsuarios.render(usuarios, mensaje));
    }

    // Devuelve un formulario para crear un nuevo usuario
    public Result formularioNuevoUsuario() {
        return ok(formCreacionUsuario.render(Form.form(Usuario.class),""));
    }

    @Transactional
    // Añade un nuevo usuario en la BD y devuelve código HTTP
    // de redirección a la página de listado de usuarios
    public Result grabaNuevoUsuario() {
        Form<Usuario> usuarioForm = Form.form(Usuario.class).bindFromRequest();
        if (usuarioForm.hasErrors()) {
            return badRequest(formCreacionUsuario.render(usuarioForm, "Hay errores en el formulario"));
        }
        Usuario usuario = usuarioForm.get();
        if (UsuarioService.findUsuarioByLogin(usuario.login) != null) {
            return badRequest(formCreacionUsuario.render(usuarioForm,  "Error: el login '" +
                                                                      usuario.login +
                                                                      "' ya existe"));
        }
        usuario = UsuarioService.grabaUsuario(usuario);
        flash("grabaUsuario", "El usuario se ha grabado correctamente");
        return redirect(controllers.routes.Usuarios.listaUsuarios());
    }

    @Transactional
    // Añade un nuevo usuario en la BD y devuelve código HTTP
    // de redirección a la página de listado de usuarios
    public Result grabaUsuarioModificado(Integer usuarioId) {
        Form<Usuario> usuarioForm = Form.form(Usuario.class).bindFromRequest();
        if (usuarioForm.hasErrors()) {
            return badRequest(formModificacionUsuario.render(usuarioForm, "Hay errores en el formulario", usuarioId));
        }
        Usuario usuario = usuarioForm.get();
        usuario.id = usuarioId;
        usuario = UsuarioService.modificaUsuario(usuario);
        flash("grabaUsuario", "El usuario se ha modificado correctamente");
        return redirect(controllers.routes.Usuarios.listaUsuarios());
    }

    @Transactional(readOnly = true)
    // Busca un usuario en la BD y devuelve un formulario con sus datos
    public Result editarUsuario(Integer id) {
        Usuario usuario = UsuarioService.findUsuario(id);
        if (usuario == null) {
            return notFound(String.format("Usuario %s no existe", id));
        }
        Form<Usuario> usuarioForm = Form.form(Usuario.class).fill(usuario);
        return ok(formModificacionUsuario.render(usuarioForm,"", id));
    }

    @Transactional(readOnly = true)
    // Busca un usuario en la BD y devuelve sus datos
    public Result detalleUsuario(Integer id) {
        Usuario usuario = UsuarioService.findUsuario(id);
        if (usuario == null) {
            return notFound(String.format("Usuario %s no existe", id));
        }
        return ok(detalleUsuario.render(usuario));
    }

    @Transactional(readOnly = true)
    // Busca un usuario en la BD y devuelve sus datos
    public Result detalleUsuarioByLogin(String login) {
        Usuario usuario = UsuarioService.findUsuarioByLogin(login);
        if (usuario == null) {
            return notFound(String.format("Usuario %s no existe", login));
        }
        return ok(detalleUsuario.render(usuario));
    }

    @Transactional
    public Result borraUsuario(Integer id) {
        if (UsuarioService.deleteUsuario(id))
            return ok();
        else
            return notFound();
    }
}
