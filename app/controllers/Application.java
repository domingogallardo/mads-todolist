package controllers;

import java.util.List;

import play.*;
import play.mvc.*;
import views.html.*;
import static play.libs.Json.*;
import play.data.Form;
import play.db.jpa.*;

import models.*;

public class Application extends Controller {
    public Result hola(String nombre) {
        return ok(hola.render(nombre));
    }

    public Result home() {
        String mensaje = flash("grabaUsuario");
        return ok(index.render(mensaje));
    }
}
