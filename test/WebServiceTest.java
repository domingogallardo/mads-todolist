import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.libs.F.*;

import play.libs.ws.*;
import play.Logger;

import static play.test.Helpers.*;
import static org.junit.Assert.*;

public class WebServiceTest {
    @Test
    public void testFormularioLogin() {
        running(testServer(3333, fakeApplication(inMemoryDatabase())), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/login").get().get(timeout);
            assertEquals(OK, response.getStatus());
            assertTrue(response.getBody().contains("<h1>Login</h1>"));
        });
    }

    @Test
    public void testdoLoginUsuarioNotFound() {
        running(testServer(3333, fakeApplication(inMemoryDatabase())), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/login")
                            .setContentType("application/x-www-form-urlencoded")
                            .post("login=domingo&password=gallardo")
                            .get(timeout);
            assertEquals(BAD_REQUEST, response.getStatus());
            assertTrue(response.getBody().contains("No existe el usuario"));
        });
    }

    @Test
    public void testRegistraNuevoUsuario() {
        running(testServer(3333, fakeApplication(inMemoryDatabase())), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/registro")
                            .setFollowRedirects(true)
                            .setContentType("application/x-www-form-urlencoded")
                            .post("login=domingo&password=gallardo&eMail=domingo.gallardo@ua.es")
                            .get(timeout);
            assertTrue(response.getBody().contains("Listado de tareas"));
        });
    }
}
